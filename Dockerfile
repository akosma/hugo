FROM docker.io/floryn90/hugo:0.123.3-ext-asciidoctor-ci
RUN wget --quiet https://github.com/Mayeu/slugify/releases/download/v1.0.2/slugify -O /usr/local/bin/slugify && chmod +x /usr/local/bin/slugify
WORKDIR /build
COPY hugo-new.sh /usr/local/bin/hugo-new.sh
ENTRYPOINT ["hugo"]
