#!/usr/bin/env bash

SLUG=$(slugify "$1")
hugo new content/posts/"$SLUG"/index.md
